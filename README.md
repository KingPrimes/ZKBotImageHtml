<div align="center">

<p align="center">
   <img src=".github/image/ico.jpg" width="200" height="200" alt="非常感谢 `别哭草莓菠菜^ ^ 微信号：Xwenlilililili_` 绘制的图像" title="非常感谢 `别哭草莓菠菜^ ^ 微信号：Xwenlilililili_` 绘制的图像">
</p>

# ZeroKingBot

<p>此库是ZeroKingBot自定义图片渲染库</p>
<p>需要配合ZeroKingBot使用</p>

[![Java CI Package](https://github.com/KingPrimes/ZeroKingBot/actions/workflows/release.yml/badge.svg)](https://github.com/KingPrimes/ZeroKingBot/actions/workflows/release.yml)
</div>

项目部署
---
- 启动ZeroKingBot会自动下载并部署


服务器资源推荐
---
- [阿里云优惠](https://www.aliyun.com/minisite/goods?userCode=8dt5pt0g&share_source=copy_link)
- [腾讯云优惠](https://cloud.tencent.com/act/pro/cps_3?fromSource=gwzcw.6688284.6688284.6688284&cps_key=ae3b8b6e55495d8bc53f2227ea0273d8)
- [腾讯云免费体验](https://cloud.tencent.com/act/free)

贡献者
---
---
- 非常感谢 `别哭草莓菠菜^ ^ 微信号：Xwenlilililili_` 绘制的图像
<!-- readme: collaborators,contributors -start -->
<!-- readme: collaborators,contributors -end -->

赞助作者
---
---
<img src=".github/image/upA-W.png" width="500"/>
